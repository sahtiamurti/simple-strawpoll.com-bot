"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
	return new (P || (P = Promise))(function (resolve, reject) {
		function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
		function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
		function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
		step((generator = generator.apply(thisArg, _arguments || [])).next());
	});
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = require("node-fetch");
const https = require("https");
function getData(url, agent, protection, id) {
	return __awaiter(this, void 0, void 0, function* () {
		const req = yield node_fetch_1.default(url, {
			method: 'GET',
			agent: protection ? agent : null
		});
		const res = yield req.text();

		// get csrftoken
		let csrftoken = res.split("csrfToken: \"")[1];
		csrftoken = csrftoken.split("\"")[0];

		// get options
		let opt = res.split("option-PKgl3EP3Rnp-1Mnwz2jAxy7\" value=\"")[1];
		opt = opt.split("\"")[0];

		// get version
		let version = res.split(",\"version\":\"")[1]
		version = version.split("\"")[0];

		const data = {
			csrftoken: csrftoken,
			option: opt,
			version: version,
		};
		return data;
	});
}
exports.default = getData;
