"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = require("node-fetch");
const util = require('util')

function sendVote(url, agent, formData, id, protection, csrftoken) {
    node_fetch_1.default(url, {
        method: 'POST',
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded'
            'Content-Type': 'text/plain;charset=UTF-8',
			'X-CSRF-TOKEN': csrftoken,
        },
        agent: protection ? agent : null,
        body: formData
    })
        .then((res) => {
        console.log(`[${String(id)}]: Vote sended!`);
		res.json().then(res3 => {
			console.log(`[${String(id)}]: ${JSON.stringify(res3)}`);
		})
    })
        .catch((err) => {
        console.log(`[${String(id)}]: Error sending the vote! ${err}`);
    });
}
exports.default = sendVote;
