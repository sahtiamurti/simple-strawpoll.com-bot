"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SocksProxyAgent = require("socks-proxy-agent");
const HttpsProxyAgent = require("https-proxy-agent");
const fs_1 = require("fs");
const querystring_1 = require("querystring");
const getData_1 = require("./getData");
const sendVote_1 = require("./sendVote");
const util = require('util')
const https = require('https')
const node_fetch_1 = require("node-fetch");
function botPoll(url, option, protection, socksFilePath, amount, urlSendVote) {
	if (!url || !url.includes('strawpoll.com'))
		throw new Error('Invalid poll url!');
	if (protection)
		if (!socksFilePath || !fs_1.existsSync(socksFilePath))
			throw new Error('Invalid socks file path!');
	const socks = protection ? fs_1.readFileSync(socksFilePath, 'utf8').trim().split('\n') : null;
	if (protection)
		for (const sock of socks) {
			// let urlSock = `http://${sock}`;
			// const agent = new HttpsProxyAgent.HttpsProxyAgent(urlSock);

			let urlSock = `socks://${sock}`;
			const agent = new SocksProxyAgent(urlSock);

			// const agent = new https.Agent({
			// 	rejectUnauthorized: false,
			// });

			const id = socks.indexOf(sock);
			console.log(`[${String(id)}]: Use Proxy : ${urlSock}`);

			node_fetch_1.default('https://api.ipify.org/?format=json', {
				method: 'GET',
				agent: protection ? agent : null,
				headers: {
					'content-type': 'application/json; charset=utf-8',
				}
			}).then((res) => {
				res.text().then(res2 => {
					console.log(`[${String(id)}]: send from ${res2}`);

					getData_1.default(url, agent, protection, id)
						.then(data => {
							let formData = {
								"pv": data.version,
								"v": {
									"id": '',
									"name": '',
									"pollVotes": [
										{
											"id": data.option,
											"value": 1,
										}
									],
									"voteType": "add",
									"token": '',
									"isEmbed": false,
								},
								"h": false,
								"ht": false,
								"token": null,
								"st": null,
							};
							formData = JSON.stringify(formData);
							sendVote_1.default(urlSendVote, agent, formData, id, protection, data.csrftoken);
						})
						.catch((e) => {
							console.log(`[${String(id)}]: Error fetching the poll data! ${e}`);
						});
				})
			}).catch((err) => {
				console.log(`[${String(id)}]: err agent : ${err}`);
			});
		}
	else
		for (let i = 0; i < amount; i++) {
			const id = i;
			node_fetch_1.default('https://api.ipify.org/?format=json', {
				method: 'GET',
				agent: protection ? agent : null,
				headers: {
					'content-type': 'application/json; charset=utf-8',
				}
			}).then((res) => {
				res.text().then(res2 => {
					console.log(`[${String(id)}]: send from ${res2}`);

					getData_1.default(url, null, protection, id)
						.then(data => {
							let formData = {
								"pv": data.version,
								"v": {
									"id": '',
									"name": '',
									"pollVotes": [
										{
											"id": data.option,
											"value": 1,
										}
									],
									"voteType": "add",
									"token": '',
									"isEmbed": false,
								},
								"h": false,
								"ht": false,
								"token": null,
								"st": null,
							};
							formData = JSON.stringify(formData);
							sendVote_1.default(urlSendVote, null, formData, id, protection, data.csrftoken);
						})
						.catch((e) => {
							console.log(`[${String(id)}]: Error fetching the poll data! ${e}`);
						});
				})
			}).catch((err) => {
				console.log(`[${String(id)}]: err agent : ${err}`);
			});
		}
}
exports.default = botPoll;
